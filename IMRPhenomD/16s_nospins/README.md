# ROQ bases of non-spinning IMRPhenomD

- `basis.hdf5` contains full-samples bases, and `basis_multiple.hdf5` does multibanded ones.
- Mass range: $`8M_\odot < \mathcal{M} < 14M_\odot,~q>0.5`$
- 3 linear bases and 2 quadratic bases are constructed over chirpmass ranges of the same widths ($`\Delta \mathcal{M} = 2M_\odot`$ for linear and $`\Delta \mathcal{M} = 3M_\odot`$ for quadratic).
- Frequency range: 20-1024 Hz
- Frequency resolution: 1/16 Hz
- `basis_addcal.hdf5` and `basis_multiband_addcal.hdf5` are trained against non-zero calibration uncertainties. The maximum errors are 10% in amplitude and 10deg in phase.
